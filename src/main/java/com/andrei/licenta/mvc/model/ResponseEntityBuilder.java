package com.andrei.licenta.mvc.model;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * MIT License
 * <p>
 * Copyright (c) 2018 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
public class ResponseEntityBuilder<T> {

    private T responseBody;
    private HttpStatus httpStatus;

    public ResponseEntityBuilder<T> setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
        return this;
    }

    public ResponseEntityBuilder<T> responseBody(T responseBody) {
        this.responseBody = responseBody;
        return this;
    }

    public ResponseEntity<T> build() {
        switch (httpStatus) {
            case ACCEPTED:
                return ResponseEntity.accepted().body(responseBody);
            case BAD_REQUEST:
                return ResponseEntity.badRequest().body(responseBody);
            case OK:
                return ResponseEntity.ok(responseBody);
            case UNAUTHORIZED:
                return ResponseEntity.status(httpStatus).body(responseBody);
            default:
                return ResponseEntity.unprocessableEntity().build();
        }
    }

}
