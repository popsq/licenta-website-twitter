package com.andrei.licenta.auth;

import com.andrei.licenta.auth.configuration.LoginAuthenticationProvider;
import com.andrei.licenta.mvc.model.ActionResult;
import com.andrei.licenta.mvc.model.GenericResponse;
import com.andrei.licenta.mvc.model.GenericResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

import static com.andrei.licenta.mvc.model.ActionResultCode.AUTH_FAILURE;
import static com.andrei.licenta.mvc.model.ActionResultCode.AUTH_SUCCESS;

/**
 * MIT License
 * <p>
 * Copyright (c) 2018 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
@Component
public class ApiAuthentication {

    private final LoginAuthenticationProvider provider;

    @Autowired
    public ApiAuthentication(LoginAuthenticationProvider provider) {
        this.provider = provider;
    }

    public GenericResponse attemptAuthentication(HttpServletRequest request) {
        try {
            this.provider.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            request.getHeader("username"),
                            request.getHeader("password")
                    )
            );
            return onAuthSuccess();
        } catch (AuthenticationException ex) {
            return onAuthFail(ex);
        }
    }

    private GenericResponse onAuthFail(Exception exception) {
        return new GenericResponseBuilder().addResult(ActionResult.buildResult(AUTH_FAILURE, exception.getMessage())).build();
    }

    private GenericResponse onAuthSuccess() {
        return new GenericResponseBuilder().addResult(ActionResult.buildResult(AUTH_SUCCESS, "Auth successful.")).build();
    }
}
