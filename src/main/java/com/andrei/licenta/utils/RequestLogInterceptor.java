package com.andrei.licenta.utils;

import org.springframework.core.Ordered;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestControllerAdvice
public class RequestLogInterceptor implements HandlerInterceptor, HandlerExceptionResolver, Ordered, LogProvider {
    @Override
    public int getOrder() {
        //We want the request log interceptor to trigger first
        return Integer.MIN_VALUE;
    }

    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {

        //We only log errors here
        logger().info(
                "Request - Error - {} | {} - Message - {}",
                httpServletRequest.getServletPath(),
                httpServletRequest.getPathInfo(),
                e.getMessage(),
                e
        );

        return null;
    }

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {

        logger().info(
                "Request - Start - {} | {}",
                httpServletRequest.getMethod(),
                httpServletRequest.getRequestURI()
        );

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
        //Do nothing
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
        logger().info(
                "Request - End - {} | {}",
                httpServletRequest.getMethod(),
                httpServletRequest.getRequestURI()
        );
    }
}
