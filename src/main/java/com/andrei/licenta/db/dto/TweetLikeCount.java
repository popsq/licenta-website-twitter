package com.andrei.licenta.db.dto;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "ttweetlikecount")
public class TweetLikeCount implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @OneToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "tweetId", unique = true, referencedColumnName = "id")
    private Tweet tweet;

    @Column(name = "likeCount", columnDefinition = "integer default 0")
    private int likeCount;

    public TweetLikeCount(){}

    public TweetLikeCount(Tweet tweet, int likeCount) {
        this.tweet = tweet;
        this.likeCount = likeCount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Tweet getTweet() {
        return tweet;
    }

    public void setTweet(Tweet tweet) {
        this.tweet = tweet;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TweetLikeCount that = (TweetLikeCount) o;
        return Objects.equals(tweet, that.tweet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tweet);
    }
}
