package com.andrei.licenta.db.dao;

import com.andrei.licenta.db.dto.Tweet;
import com.andrei.licenta.db.dto.TweetLike;
import com.andrei.licenta.db.dto.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TweetLikeDAO extends JpaRepository<TweetLike, Long> {

    TweetLike findByTweetAndUser(Tweet tweet, User user);

}
